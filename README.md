This QT is to measure SF of boosted W/Z/H taggers using V+jets/Z+jets events.

Topology
==
![](https://codimd.web.cern.ch/uploads/upload_a8846d482ae4d26db10ac5c9aeb69782.png)

Definition of the SF
==
![](https://codimd.web.cern.ch/uploads/upload_132791caf4e31536d01e92376b974af2.png)

Workflow
==
There are 3 steps to measure \mu^{tagged}.

Step1: [VZjetsSF-ATOPNtuple](https://gitlab.cern.ch/yajun/vzjetssf-atopntuple)

Step2: [VZjetsSF-FinalSelection](https://gitlab.cern.ch/yajun/vzjetssf-finalselection)

Step3: VZjetsSF-Fitter
